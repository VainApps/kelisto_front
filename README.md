# KELISTO TEST BT MANUEL LUCENA  - MARCH 2018

This is the result for a coding test proposed by [kelisto](https://kelisto.es) for a backend developer position

It is written on HTML5, CSS3, using Bootstrap 3 using a basic template from [paintstrap](https://paintstrap.com)

In order to get it running, please run this commands:

```
git clone https://gitlab.com/VainApps/kelisto_frontend.git

```

# WHY DID I TAKE THIS APPROACH?

Wanted to make it to look like the pdf provided, although frontend is not really my strong point. I also tried to use as little JS as possible, taking advantage of the benefits of using bootstrap. Solution took me around 4 hours to complete.